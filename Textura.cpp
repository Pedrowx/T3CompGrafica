// **********************************************************************
// PUCRS/Escola Polit�cnica
// COMPUTA��O GR�FICA
//
// Programa b�sico para criar aplicacoes 3D em OpenGL
//
// Marcio Sarroglia Pinho
// pinho@pucrs.br
// **********************************************************************

#include <iostream>
#include <cmath>
#include <ctime>
#include <fstream>

using namespace std;

#ifdef WIN32
#include <windows.h>
#include "gl\glut.h"
    static DWORD last_idle_time;
#else
    #include <sys/time.h>
    static struct timeval last_idle_time;
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>
#endif

#include "SOIL/SOIL.h"

#include "TextureClass.h"



GLfloat AspectRatio, Angulo=0;
GLuint TEX1, TEX2, TEX3, TEX4;

//Controle de Camera
float eyeX = 0;
float eyeY = 1.8;
float eyeZ = 1;

float centerX = 0;
float centerY = 0;
float centerZ = 0;

// *********************************************************************
//   ESTRUTURAS A SEREM USADAS PARA ARMAZENAR UM OBJETO 3D
// *********************************************************************

typedef struct  // Struct para armazenar um ponto
{
    float X,Y,Z;
    void Set(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }
    void Imprime()
    {
        cout << "X: " << X << " Y: " << Y << " Z: " << Z;
    }
} TPoint;


typedef struct // Struct para armazenar um tri�ngulo
{
    TPoint P1, P2, P3;
    TPoint normal;
    COLORREF ColRef;
    void imprime()
    {
        cout << "P1 ";  P1.Imprime(); cout << endl;
        cout << "P2 ";  P2.Imprime(); cout << endl;
        cout << "P3 ";  P3.Imprime(); cout << endl;
    }
} TTriangle;

// Classe para armazenar um objeto 3D
class Objeto3D
{
    TTriangle *faces; // vetor de faces
    unsigned int nFaces; // Variavel que armazena o numero de faces do objeto
public:
    Objeto3D()
    {
        nFaces = 0;
        faces = NULL;
    }
    unsigned int getNFaces()
    {
        return nFaces;
    }
    float menorX()
    {
        float x = faces[0].P1.X;
        for(int i=0; i<nFaces; i++){
            if(faces[i].P1.X<x){
                x=faces[i].P1.X;
            }
            if(faces[i].P2.X<x){
                x=faces[i].P2.X;
            }
            if(faces[i].P3.X<x){
                x=faces[i].P3.X;
            }
        }
        //printf("menorX: %f\n", x);
        return x;
    }
    float menorY()
    {
        float y = faces[0].P1.Y;
        for(int i=0; i<nFaces; i++){
            if(faces[i].P1.Y<y){
                y=faces[i].P1.Y;
            }
            if(faces[i].P2.Y<y){
                y=faces[i].P2.Y;
            }
            if(faces[i].P3.Y<y){
                y=faces[i].P3.Y;
            }
        }
        //printf("menorY: %f\n", y);
        return y;
    }
    float maiorZ()
    {
        float z = faces[0].P1.Z;
        for(int i=0; i<nFaces; i++){
            if(faces[i].P1.Z>z){
                z=faces[i].P1.Z;
            }
            if(faces[i].P2.Z>z){
                z=faces[i].P2.Z;
            }
            if(faces[i].P3.Z>z){
                z=faces[i].P3.Z;
            }
        }
        //printf("maiorZ: %f\n", z);
        return z;
    }
    void LeObjeto (char *Nome); // implementado fora da classe
    void ExibeObjeto(); // implementado fora da classe
};
Objeto3D *MundoVirtual;

// Rotina que faz um produto vetorial
void ProdVetorial (TPoint v1, TPoint v2, TPoint &vresult)
    {
        vresult.X = v1.Y * v2.Z - (v1.Z * v2.Y);
        vresult.Y = v1.Z * v2.X - (v1.X * v2.Z);
        vresult.Z = v1.X * v2.Y - (v1.Y * v2.X);
    }

// Esta rotina tem como funcao calcular um vetor unitario
void VetUnitario(TPoint &vet)
    {
        float modulo;

        modulo = sqrt (vet.X * vet.X + vet.Y * vet.Y + vet.Z * vet.Z);

        if (modulo == 0.0) return;

        vet.X /= modulo;
        vet.Y /= modulo;
        vet.Z /= modulo;
    }

// *********************************************************************
//   DESCRICAO DO FORMATO ".TRI"
// *********************************************************************

/*
 A primeira linha do arquivo cont�m o n�mero de tri�ngulos que forma o objeto
 As demais linhas descrevemas os tri�ngulos:
 x1 y1 z1 x2 y2 z2 x3 y3 z3
 onde
 x1 y1 z1 : primeiro v�rtice do tri�ngulo
 x2 y2 z2 : segundo v�rtice do tri�ngulo
 x3 y3 z3 : terceiro v�rtice do tri�ngulo
 Um exemplo com dois tri�ngulos:
 2
 0 0 10 10 0 10 5 10 10
 10 0 10 20 0 10 15 10 10

 */
// **********************************************************************
// void LeObjeto(char *Nome)
// **********************************************************************
void Objeto3D::LeObjeto (char *Nome)
{
    // ***************
    // Exercicio
    //      complete esta rotina fazendo a leitura do objeto
    // ***************

    ifstream arq;
    arq.open(Nome, ios::in);
    if (!arq)
    {
        cout << "Erro na abertura do arquivo " << Nome << "." << endl;
        exit(1);
    }
    arq >> nFaces;
    faces = new TTriangle[nFaces];
    float x,y,z;
    int color;
    for (int i=0;i<nFaces;i++)
    {
        // Le os tr�s v�rtices
        arq >> x >> y >> z; // Vertice 1
        faces[i].P1.Set(x,y,z);
        arq >> x >> y >> z; // Vertice 2
        faces[i].P2.Set(x,y,z);
        arq >> x >> y >> z; // Vertice 3
        faces[i].P3.Set(x,y,z);
        arq >> hex >> color;
        faces[i].ColRef=color;
        //cout << i << ": ";
        //faces[i].imprime();
    }

    x = menorX();
    y = menorY();
    z = maiorZ();
    for(int i=0;i<nFaces;i++)
    {
        faces[i].P1.Set(faces[i].P1.X-x, faces[i].P1.Y-y, faces[i].P1.Z-z);
        faces[i].P2.Set(faces[i].P2.X-x, faces[i].P2.Y-y, faces[i].P2.Z-z);
        faces[i].P3.Set(faces[i].P3.X-x, faces[i].P3.Y-y, faces[i].P3.Z-z);

        TPoint vetAB;
        TPoint vetAC;
        TPoint norm;
        vetAB.Set(faces[i].P2.X-faces[i].P1.X, faces[i].P2.Y-faces[i].P1.Y, faces[i].P2.Z-faces[i].P1.Z);
        vetAC.Set(faces[i].P3.X-faces[i].P1.X, faces[i].P3.Y-faces[i].P1.Y, faces[i].P3.Z-faces[i].P1.Z);
        ProdVetorial(vetAB,vetAC, norm);
        VetUnitario(norm);

        faces[i].normal.Set(norm.X, norm.Y, norm.Z);
    }
}

// **********************************************************************
// void ExibeObjeto (TTriangle **Objeto)
// **********************************************************************
void Objeto3D::ExibeObjeto ()
{
    glPushMatrix();
    for(int i=0; i<nFaces; i++){
        //glColor3f(GetRValue(faces[i].ColRef), GetGValue(faces[i].ColRef), GetBValue(faces[i].ColRef));
        glBegin(GL_TRIANGLES);
            glNormal3f(faces[i].normal.X, faces[i].normal.Y, faces[i].normal.Z);
            glVertex3f(faces[i].P1.X, faces[i].P1.Y, faces[i].P1.Z);
            glVertex3f(faces[i].P2.X, faces[i].P2.Y, faces[i].P2.Z);
            glVertex3f(faces[i].P3.X, faces[i].P3.Y, faces[i].P3.Z);
        glEnd();
        }

    glPopMatrix();
}

// **********************************************************************
//  void initTexture(void)
//        Define a textura a ser usada
//
// **********************************************************************
void initTexture (void)
{
    TEX1 = LoadTexture ("GramaComContorno.jpg");
    TEX2 = LoadTexture ("TijolosComContorno.jpg");
}


// **********************************************************************
//  void DefineLuz(void)
//
//
// **********************************************************************
void DefineLuz(void)
{
  // Define cores para um objeto dourado
  GLfloat LuzAmbiente[]   = {0.4, 0.4, 0.4f } ;
  GLfloat LuzDifusa[]   = {0.7, 0.7, 0.7};
  GLfloat LuzEspecular[] = {0.9f, 0.9f, 0.9 };
  GLfloat PosicaoLuz0[]  = {3.0f, 3.0f, 0.0f };
  GLfloat Especularidade[] = {1.0f, 1.0f, 1.0f};

   // ****************  Fonte de Luz 0

  glEnable ( GL_COLOR_MATERIAL );

   // Habilita o uso de ilumina��o
  glEnable(GL_LIGHTING);

  // Ativa o uso da luz ambiente
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LuzAmbiente);
  // Define os parametros da luz n�mero Zero
  glLightfv(GL_LIGHT0, GL_AMBIENT, LuzAmbiente);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, LuzDifusa  );
  glLightfv(GL_LIGHT0, GL_SPECULAR, LuzEspecular  );
  glLightfv(GL_LIGHT0, GL_POSITION, PosicaoLuz0 );
  glEnable(GL_LIGHT0);

  // Ativa o "Color Tracking"
  glEnable(GL_COLOR_MATERIAL);

  // Define a reflectancia do material
  glMaterialfv(GL_FRONT,GL_SPECULAR, Especularidade);

  // Define a concentra��oo do brilho.
  // Quanto maior o valor do Segundo parametro, mais
  // concentrado ser� o brilho. (Valores v�lidos: de 0 a 128)
  glMateriali(GL_FRONT,GL_SHININESS,51);

}


// **********************************************************************
//  void init(void)
//		Inicializa os par�metros globais de OpenGL
//
// **********************************************************************
void init(void)
{
	glClearColor(0.1f, 0.7f, 0.8f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glColorMaterial ( GL_FRONT, GL_AMBIENT_AND_DIFFUSE );
	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glEnable ( GL_CULL_FACE );

    // Obtem o tempo inicial
#ifdef WIN32
    last_idle_time = GetTickCount();
#else
    gettimeofday (&last_idle_time, NULL);
#endif

    initTexture();

}
// **********************************************************************
//  void PosicUser()
//
//
// **********************************************************************
void PosicUser()
{
	// Set the clipping volume
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90,AspectRatio,0.01,200);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyeX, eyeY, eyeZ,
		      centerX, centerY, centerZ,
			  0.0f,1.0f,0.0f);

}
// **********************************************************************
//  void reshape( int w, int h )
//		trata o redimensionamento da janela OpenGL
//
// **********************************************************************
void reshape( int w, int h )
{

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	AspectRatio = 1.0f * w / h;
	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	// Set the viewport to be the entire window
    glViewport(0, 0, w, h);
    //cout << "Largura" << w << endl;

	PosicUser();

}
// **********************************************************************
//   void DesenhaCubo (GLuint nro_da_textura)
//
//
// **********************************************************************

void DesenhaCactus()
{
    glPushMatrix();
        glColor3f(0.0f, 0.25f, 0.5f);
        glScalef(0.03f, 0.03f, 0.03f);
        MundoVirtual[0].ExibeObjeto();
    glPopMatrix();
}

void DesenhaCaminhao()
{
    glPushMatrix();
        glColor3f(0.5f, 0.2f, 0.0f);
        glScalef(0.1f, 0.1f, 0.1f);
        MundoVirtual[1].ExibeObjeto();
    glPopMatrix();
}

void DesenhaArvore2()
{
    glPushMatrix();
        glColor3f(0.0f, 0.25f, 0.5f);
        glScalef(0.05f, 0.05f, 0.05f);
        MundoVirtual[2].ExibeObjeto();
    glPopMatrix();
}

void DesenhaDog()
{
    glPushMatrix();
        glColor3f(0.0f, 0.25f, 0.5f);
        glScalef(0.05f, 0.05f, 0.05f);
        MundoVirtual[3].ExibeObjeto();
    glPopMatrix();
}

void DesenhaCaminhao2()
{
    glPushMatrix();
        glColor3f(0.5f, 0.2f, 0.0f);
        glScalef(0.1f, 0.1f, 0.1f);
        MundoVirtual[4].ExibeObjeto();
    glPopMatrix();
}

void DesenhaArvore()
{
    glPushMatrix();
        glColor3f(0.0f, 0.25f, 0.5f);
        glScalef(0.08f, 0.08f, 0.08f);
        MundoVirtual[5].ExibeObjeto();
    glPopMatrix();
}

void DesenhaDog2()
{
    glPushMatrix();
        glColor3f(0.0f, 0.25f, 0.5f);
        glScalef(0.1f, 0.1f, 0.1f);
        MundoVirtual[6].ExibeObjeto();
    glPopMatrix();
}

void DesenhaIgreja()
{
    glPushMatrix();
        glColor3f(0.5f, 0.2f, 0.0f);
        glScalef(0.1f, 0.1f, 0.1f);
        MundoVirtual[7].ExibeObjeto();
    glPopMatrix();
}

void DesenhaMoto()
{
    glPushMatrix();
        glColor3f(0.5f, 0.2f, 0.0f);
        glScalef(0.03f, 0.03f, 0.03f);
        MundoVirtual[8].ExibeObjeto();
    glPopMatrix();
}

void DesenhaMoto2()
{
    glPushMatrix();
        glColor3f(0.5f, 0.2f, 0.0f);
        glScalef(0.05f, 0.05f, 0.05f);
        MundoVirtual[9].ExibeObjeto();
    glPopMatrix();
}

void DesenhaCubo ()
{
    glPushMatrix();
        glBegin ( GL_QUADS );
        // Top Face
        glTexCoord2f(0.0f, 0.1f);
        glVertex3f(-0.1f,  0.1f, -0.1f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.1f,  0.1f,  0.1f);
        glTexCoord2f(0.1f, 0.0f);
        glVertex3f( 0.1f,  0.1f,  0.1f);
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f( 0.1f,  0.1f, -0.1f);
        // Front Face
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.1f, -0.1f,  0.1f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( 0.1f, -0.1f,  0.1f);
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f( 0.1f,  0.1f,  0.1f);
        glTexCoord2f(0.0f, 0.1f);
        glVertex3f(-0.1f, 0.1f,  0.1f);
        // Back Face
        glTexCoord2f(0.1f, 0.0f);
        glVertex3f(-0.1f, -0.1f, -0.1f);
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f(-0.1f,  0.1f, -0.1f);
        glTexCoord2f(0.0f, 0.1f);
        glVertex3f( 0.1f,  0.1f, -0.1f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( 0.1f, -0.1f, -0.1f);
        // Bottom Face
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f(-0.1f, -0.1f, -0.1f);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f( 0.1f, -0.1f, -0.1f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( 0.1f, -0.1f,  0.1f);
        glTexCoord2f(0.1f, 0.0f);
        glVertex3f(-0.1f, -0.1f,  0.1f);
        // Right face
        glTexCoord2f(0.1f, 0.0f);
        glVertex3f( 0.1f, -0.1f, -0.1f);
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f( 0.1f,  0.1f, -0.1f);
        glTexCoord2f(0.0f, 0.1f);
        glVertex3f( 0.1f,  0.1f,  0.1f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( 0.1f, -0.1f,  0.1f);
        // Left Face
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.1f, -0.1f, -0.1f);
        glTexCoord2f(0.1f, 0.0f);
        glVertex3f(-0.1f, -0.1f,  0.1f);
        glTexCoord2f(0.1f, 0.1f);
        glVertex3f(-0.1f,  0.1f,  0.1f);
        glTexCoord2f(0.0f, 0.1f);
        glVertex3f(-0.1f,  0.1f, -0.1f);
        glEnd();
    glPopMatrix();
}

void DesenhaLinhaCubosTijolos(int tam)
{
    glPushMatrix();
    for(int i=0; i<tam; i++){
        glTranslatef(0.0f, 0.2f, 0.0f);
        DesenhaCubo();
    }
    glPopMatrix();
}

void DesenhaGradeTijolos(int tamX, int tamY)
{
    glPushMatrix();
    for(int i=0; i<tamY; i++){
        DesenhaLinhaCubosTijolos(tamX);
        glTranslatef(0.0f, 0.0f, -0.2f);
    }
    glPopMatrix();
}

void DesenhaLinhaCubosGrama(int tam)
{
    glPushMatrix();
    for(int i=0; i<tam; i++){
        DesenhaCubo();
        glTranslatef(0.0f, 0.0f, -0.2f);
    }
    glPopMatrix();
}

void DesenhaGradeGrama(int tamX, int tamY)
{
    glPushMatrix();
    for(int i=0; i<tamY; i++){
        DesenhaLinhaCubosGrama(tamX);
        glTranslatef(0.2f, 0.0f, 0.0f);
    }
    glPopMatrix();
}
// **********************************************************************
//  void display( void )
//
//
// **********************************************************************
void display( void )
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	DefineLuz();

	PosicUser();

	glMatrixMode(GL_MODELVIEW);

	//glRotatef(Angulo,0,1,0);
    glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
        glBindTexture (GL_TEXTURE_2D, TEX1);
        DesenhaGradeGrama(25,50);
	glPopMatrix();

	glPushMatrix();
        glBindTexture (GL_TEXTURE_2D, TEX2);
        glTranslatef(4.8f, 0.0f, 0.0f);
        DesenhaGradeTijolos(6,25);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
        glTranslatef(0.0f, 0.0f, -2.5f);
        DesenhaCactus();
	glPopMatrix();

	glPushMatrix();
        //glTranslatef(0.0f, 0.0f, -2.5f);
        glTranslatef(0.0f, 0.1f, 0.0f);
        //DesenhaCaminhao();
        //DesenhaDog();
        //DesenhaArvore();
        //DesenhaArvore2();
        //DesenhaDog2();
        //DesenhaIgreja();
        //DesenhaMoto();
        //DesenhaMoto2();
        DesenhaCaminhao2();

	glPopMatrix();


	glutSwapBuffers();
}

// **********************************************************************
//  void animate ( unsigned char key, int x, int y )
//
//
// **********************************************************************
void animate()
{
    static float dt;
    static float AccumTime=0;

#ifdef _WIN32
    DWORD time_now;
    time_now = GetTickCount();
    dt = (float) (time_now - last_idle_time) / 1000.0;
#else
    // Figure out time elapsed since last call to idle function
    struct timeval time_now;
    gettimeofday(&time_now, NULL);
    dt = (float)(time_now.tv_sec  - last_idle_time.tv_sec) +
    1.0e-6*(time_now.tv_usec - last_idle_time.tv_usec);
#endif
    AccumTime +=dt;
    if (AccumTime >=3) // imprime o FPS a cada 3 segundos
    {
        cout << 1.0/dt << " FPS"<< endl;
        AccumTime = 0;
    }
    //cout << "AccumTime: " << AccumTime << endl;
    // Anima cubos
    Angulo++;
    // Sa;va o tempo para o pr�ximo ciclo de rendering
    last_idle_time = time_now;

        //if  (GetAsyncKeyState(32) & 0x8000) != 0)
          //  cout << "Espaco Pressionado" << endl;

    // Redesenha
    glutPostRedisplay();
}

// **********************************************************************
//  void keyboard ( unsigned char key, int x, int y )
//
//
// **********************************************************************
void keyboard ( unsigned char key, int x, int y )
{
	switch ( key )
	{
    case 27:        // Termina o programa qdo
      exit ( 0 );   // a tecla ESC for pressionada
      break;
    case 'w':
        eyeZ-=0.05;
        centerZ-=0.05;
        break;
    case 'a':
        eyeX-=0.05;
        centerX-=0.05;
        break;
    case 's':
        eyeZ+=0.05;
        centerZ+=0.05;
        break;
    case 'd':
        eyeX+=0.05;
        centerX+=0.05;
        break;
    case 't':
        eyeY+=0.05;
        centerY-=0.05;
        break;
    case 'y':
        eyeY-=0.05;
        centerY+=0.05;
        break;
    default:
            cout << key;
      break;
  }
}

// **********************************************************************
//  void arrow_keys ( int a_keys, int x, int y )
//
//
// **********************************************************************
void arrow_keys ( int a_keys, int x, int y )
{
	switch ( a_keys )
	{
		case GLUT_KEY_UP:       // When Up Arrow Is Pressed...
			glutFullScreen ( ); // Go Into Full Screen Mode
			break;
	    case GLUT_KEY_DOWN:     // When Down Arrow Is Pressed...
			glutInitWindowSize  ( 700, 500 );
			break;
		default:
			break;
	}
}

// **********************************************************************
//  void main ( int argc, char** argv )
//
//
// **********************************************************************
int main ( int argc, char** argv )
{
	glutInit            ( &argc, argv );
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB );
	glutInitWindowPosition (0,0);
	glutInitWindowSize  ( 700, 500 );
	glutCreateWindow    ( "Computacao Grafica - Exemplo Basico 3D" );

	// aloca mem�ria para 5 objetos
    MundoVirtual = new Objeto3D[10];
    // carrega o objeto 0
    MundoVirtual[0].LeObjeto ("TRIs/cactus.tri");
    MundoVirtual[1].LeObjeto ("TRIs/caminhao.tri");
    MundoVirtual[2].LeObjeto("TRIs/tree_1.tri");
    MundoVirtual[3].LeObjeto("TRIs/dog.tri");
    MundoVirtual[4].LeObjeto("TRIs/caminhao_2.tri");
    MundoVirtual[5].LeObjeto("TRIs/tree.tri");
    MundoVirtual[6].LeObjeto("TRIs/dog_2.tri");
    MundoVirtual[7].LeObjeto("TRIs/igreja.tri");
    MundoVirtual[8].LeObjeto("TRIs/moto.tri");
    MundoVirtual[9].LeObjeto("TRIs/moto_2.tri");

	init ();
    //system("pwd");

	glutDisplayFunc ( display );
	glutReshapeFunc ( reshape );
	glutKeyboardFunc ( keyboard );
	glutSpecialFunc ( arrow_keys );
	glutIdleFunc ( animate );

	glutMainLoop ( );
	return 0;
}



